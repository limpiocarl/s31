const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes");

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect(
  "mongodb+srv://admin:admin131@zuittbootcamp.0e0ak.mongodb.net/session30?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));

db.once("open", () => console.log("Connected to the cloud database"));

app.use("/tasks", taskRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));
